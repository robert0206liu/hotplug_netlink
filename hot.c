#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#define UEVENT_BUFFER_SIZE 2048

int exist = -1;

int main(void) {
	struct sockaddr_nl client;
	struct timeval tv;
	int CppLive, rcvlen, ret;
	fd_set fds;
	int buffer[UEVENT_BUFFER_SIZE] = { 0 };
	int i;
	CppLive = socket(AF_NETLINK, SOCK_RAW, NETLINK_KOBJECT_UEVENT);
	memset(&client, 0, sizeof(client));
	client.nl_family = AF_NETLINK;
	client.nl_pid = getpid();
	client.nl_groups = NETLINK_KOBJECT_UEVENT; /* receive broadcast message*/
	setsockopt(CppLive, SOL_SOCKET, SO_RCVBUF, &buffer, sizeof(buffer));
	bind(CppLive, (struct sockaddr*)&client, sizeof(client));
	
	while (1) {
		char buf[UEVENT_BUFFER_SIZE*2] = { 0 };
		FD_ZERO(&fds);
		FD_SET(CppLive, &fds);
		tv.tv_sec = 0;
		tv.tv_usec = 100 * 1000;
		ret = select(CppLive + 1, &fds, NULL, NULL, &tv);
		if(ret < 0)
			continue;
		if(!(ret > 0 && FD_ISSET(CppLive, &fds)))
			continue;
		
		/* receive data */
		rcvlen = recv(CppLive, &buf, sizeof(buf), 0);
		for(i=0;i<rcvlen;i++) {
			if(*(buf+i)=='\0') {
				buf[i]='\n';
			}
		}
		if (rcvlen > 0) {
			printf("%s\n", buf);
			  /*if(((strstr(buf, "ACTION=add"))!=NULL) && ((strstr(buf,"ID_USB_DRIVER=ax88179_178a")) != NULL)) {
				if (exist != 1) {
					exist = 1;
					printf("Add\n");
				}
			}
			if(((strstr(buf, "ACTION=remove"))!=NULL) && ((strstr(buf,"ID_USB_DRIVER=ax88179_178a")) != NULL)) {
				if (exist != 0) {
					exist = 0;
					printf("Remove\n");
				}
			}*/
		}
	} // End while
	close(CppLive);
	return 0;
}
